import org.junit.jupiter.api.Test;
import org.twitter.FollowStatus;
import org.twitter.ReceiverEndPoint;
import org.twitter.SenderEndPoint;
import org.twitter.Twitter;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.twitter.FollowStatus.ALREADY_FOLLOWING;
import static org.twitter.FollowStatus.SUCCESS;

public class TwitterTest {
    private final ReceiverEndPoint receiverEndPoint = mock(ReceiverEndPoint.class);
    private SenderEndPoint endPoint;
    private Twitter twitter = new Twitter();
    @Test
    public void shouldBeAbleToAuthenticateUser() {
        logon();
    }

    @Test
    public void shouldNotAuthenticateUnknownUser() {
        final Optional<SenderEndPoint> endPoint = twitter.onLogon(TestData.NOT_A_USER, TestData.PASSWORD, receiverEndPoint);
        assertFalse(endPoint.isPresent());
    }


    @Test
    public void shouldNotAuthenticateUserWithWrongPassword() {
        final Optional<SenderEndPoint> endPoint = twitter.onLogon(TestData.USER_ID, "wrong password", receiverEndPoint);
        assertFalse(endPoint.isPresent());
    }

    @Test
    public void shouldFollowValidUser() {
        logon();

        final FollowStatus followStatus = endPoint.onFollow(TestData.OTHER_USER_ID);

        assertEquals(FollowStatus.SUCCESS, followStatus);
    }

    @Test
    public void shouldNotDuplicateFollowUser() {
        logon();
        endPoint.onFollow(TestData.OTHER_USER_ID);

        final FollowStatus followStatus = endPoint.onFollow(TestData.OTHER_USER_ID);
        assertEquals(ALREADY_FOLLOWING, followStatus);
    }

    private void logon() {
        this.endPoint = this.logon(TestData.USER_ID, receiverEndPoint).orElse(null);
    }

    private Optional<SenderEndPoint> logon(final String userId, final ReceiverEndPoint receiverEndPoint) {
        Optional<SenderEndPoint> endPoint = twitter.onLogon(userId, TestData.PASSWORD, receiverEndPoint);
        assertTrue(endPoint.isPresent(), "Failed to logon");
        return Optional.of(endPoint.get());
    }


}


