public class TestData {
    static final String OTHER_USER_ID = "AnotherValidUser";
    static final String USER_ID = "ValidUser";

    static final String NOT_A_USER = "NotAUser";

    static final String PASSWORD = "password";
}

