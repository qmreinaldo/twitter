package org.twitter;

public interface ReceiverEndPoint {
    void onTweet(Tweet tweet);
}
