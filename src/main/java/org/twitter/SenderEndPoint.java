package org.twitter;

public class SenderEndPoint {
    private final User user;
    private final Twitter twitter;

    SenderEndPoint(final User user, final Twitter twitter) {
        this.user = user;
        this.twitter = twitter;
    }

    public FollowStatus onFollow(final String userIdToFollow) {
        return twitter.onFollow(user, userIdToFollow);
    }
}
