package org.twitter;

public class User {
    private final String id;
    private final byte[] password;

    public User(String id, byte[] password) {
        this.id = id;
        this.password = password;
    }
}
