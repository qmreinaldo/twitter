package org.twitter;

import java.util.Optional;

public class Twitter {
    public Optional<SenderEndPoint> onLogon(
            final String userId, final String password, final ReceiverEndPoint receiver) {
            return Optional.empty();
    }

    public FollowStatus onFollow(final User follow, final String userIdToFollow) {
        return FollowStatus.SUCCESS;
    }
}

