package org.twitter;

public final class Tweet {
    private final String id;
    private final String senderId;
    private final String content;

    public Tweet(final String id, final String senderId, final String content) {
        this.id = id;
        this.senderId = senderId;
        this.content = content;
    }
}
